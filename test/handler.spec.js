const {parseResponse, buildHandler} = require('../handler')

describe('Handler', () => {
  let res
  beforeEach(() => {
    res = {
      setHeader: jest.fn(),
      end: jest.fn()
    }
  })


  it('Should return 404 if url not found', async () => {
    const handler = buildHandler([])
    const req = {url: '/'}

    await handler(req, res)

    expect(res.statusCode).toBe(404)
    expect(res.setHeader).toHaveBeenCalledWith('Content-Type', 'text/plain')
  })


  it('Should return 500 on exception', async () => {
    const throwError = () => {throw Error('e')}
    const handler = buildHandler([
      {re: /^\/test$/, fn: throwError}
    ])

    const req = {url: '/test'}
    await handler(req, res)
    expect(res.statusCode).toBe(500)
  })


  it('Should correctly call handler', async () => {
    const jsonData = 'json-data'
    const handler = buildHandler([
      {re: /.*/, fn: () => ({data: jsonData})}
    ])

    const req = {url: ''}
    await handler(req, res)
    expect(res.statusCode).toBe(200)
    expect(res.end).toHaveBeenCalledWith(JSON.stringify(jsonData))
  })

  it('Should correctly set status code', () => {
    parseResponse({
      statusCode: 123
    }, res)
    expect(res.statusCode).toBe(123)
  })


  it('Should set response headers', () => {
    parseResponse({
      headers: {
        'Content-Type': 'text/plain',
        'Accept': 'application/json'
      }
    }, res)

    expect(res.setHeader).toHaveBeenCalledTimes(2)
    expect(res.setHeader).toHaveBeenCalledWith('Content-Type', 'text/plain')
    expect(res.setHeader).toHaveBeenCalledWith('Accept', 'application/json')
  })


  it('Should correctly send response headers when data provided', () => {
    parseResponse({
      headers: {
        'Content-Type': 'text/plain'
      },
      data: 'test'
    }, res)

    expect(res.setHeader).toHaveBeenCalledTimes(1)
    expect(res.setHeader).toHaveBeenCalledWith('Content-Type', 'text/plain')
  })


  it('Should not parse to json if content-type is not application/json', () => {
    parseResponse({
      headers: {
        'Content-type': 'text/plain'
      },
      data: 'a test message'
    }, res)

    expect(res.end).toHaveBeenCalledWith('a test message')
  })


  it('Should set content-type as application/json when not provided in headers', () => {
    parseResponse({
      headers: {
        'Accept': 'text/plain'
      }
    }, res)
    expect(res.setHeader).toHaveBeenCalledWith('Content-Type', 'application/json')
  })


  it('Should support async views', async () => {
    const asyncMock = jest.fn().mockResolvedValue({data: 'example'})
    const handler = buildHandler([
      { re: /.*/, fn: asyncMock}
    ])
    await handler({url: ''}, res)

    expect(res.end).toHaveBeenCalledWith('"example"')
  })
})
