const {auth} = require('../auth')

describe('Auth', () => {
  let req
  beforeEach(() => {
    req = {
      method: 'POST'
    }
  })


  it('Should not authenticate if credentials not provided', () => {
    const response = auth(req)

    expect(response.statusCode).toBe(400)
    expect(response.data.authenticated).toBe(false)
  })


  it('Should not allow GET request', () => {
    req.method = 'GET'
    const response = auth(req)

    expect(response.statusCode).toBe(401)
  })
})
