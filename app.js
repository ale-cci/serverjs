'use strict'
const http = require('http')
const urls = require('./urls')
const {buildHandler} = require('./handler')
const {SERVER_PORT, SERVER_HOST} = require('./settings')


const server = http.createServer(
  buildHandler(urls)
)

server.listen(SERVER_PORT, SERVER_HOST, () => {
  console.log(`Server running at http://${SERVER_HOST}:${SERVER_PORT}/`)
})
