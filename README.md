# Node-Server
```
/app.js             # Main
/urls.js            # Urls served by app
/views.js           # Views used to serve request, registered with urls
/auth.js            # User authentication
```

### Commands (look package.json)
Run test one time
```sh
$ npm run test
```

Run test in watch mode
```sh
$ npx jest --watch
```

Start server + autoreload on file save
```sh
$ npm start
```


### TODO
* add database connection (mongodb should be already added to the dependencies)
* finish authentication module
