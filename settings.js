const Settings = {
  SERVER_PORT: process.env.SERVER_PORT || 3000,
  SERVER_HOST: '127.0.0.1',
}

module.exports = Settings
