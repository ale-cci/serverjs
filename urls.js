const views = require('./views')
const {auth} = require('./auth')

const urls = [
  { re: /^\/test$/, fn: views.test},
  { re: /^\/auth$/, fn: auth},
  { re: /^\/?$/,    fn: views.index}
]

module.exports = urls
