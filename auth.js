const auth = (req) => {
  if (req.method !== 'POST') {
    return {
      statusCode: 401,
      data: {
        error: `Unauthorized method: ${req.method}`
      }
    }
  }

  return {
    statusCode: 400,
    data: {
      error: 'Wrong username or password',
      authenticated: false
    }
  }
}

module.exports = {auth}
