// Server listener response handler
const buildHandler = (urls) => {
  return async (req, res) => {
    const responder = urls.find(url => url.re.test(req.url))

    if (responder === undefined) {
      do404(res)
    } else {

      try {
        const response = await Promise.resolve(responder.fn(req))
        parseResponse(response, res)
      } catch (e) {
        do500(res, e)
      }

    }
  }
}


const parseResponse = (content, res) => {
  res.statusCode = content.statusCode || 200

  let contentType
  // Set content.headers
  if (content.headers) {
    Object.entries(content.headers)
      .forEach(
        ([key, value]) => res.setHeader(key, value)
      )

    Object.entries(content.headers)
      .find(([key, value]) => {
        if (key.toLowerCase() !== 'content-type')
          return false

        contentType = value
        return true
      })
  }

  // Set content type if not specified
  if (!contentType) {
    res.setHeader('Content-Type', 'application/json')
    contentType = 'application/json'
  }


  if (content.data === undefined) {
    console.warn('Unparseable content')
    res.end('')
    return
  }


  let data = content.data
  if (contentType?.toLowerCase() === 'application/json')
    data = JSON.stringify(data)

  res.end(data)
}


const do404 = (res) => {
  res.statusCode = 404
  res.setHeader('Content-Type', 'text/plain')
  res.end('Page not found')
}


const do500 = (res, exception) => {
  console.error(exception)
  res.statusCode = 500
  res.setHeader('Content-Type', 'text/plain')
  res.end('Exception occurred while executing responder')
}

module.exports = {
  buildHandler,
  parseResponse
}
