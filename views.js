const test = () => {
  return {
    data: 'test.js'
  }
}


const index = () => {
  return {
    headers: {
      'Content-type': 'text/html'
    },
    data: '<title>test</title> <p>tested html <b> should</b> be tested</p>'
  }
}


module.exports = {
  test,
  index
}
