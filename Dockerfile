FROM node:15.8.0-alpine3.10 as image


RUN adduser --no-create-home --home /home --disabled-password web
RUN chown -R web:web /home/
USER web

WORKDIR /home/

COPY --chown=web:web package.json .
COPY --chown=web:web package-lock.json .
RUN npm install && npm install --save-dev

CMD npm start
